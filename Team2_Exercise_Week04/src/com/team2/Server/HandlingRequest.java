package com.team2.Server;

import com.team2.Client.ByteArray;
import com.team2.Client.Common.Properties;
import com.team2.DataTransfer.Data;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Class nhận request từ client qua socket
 */
public class HandlingRequest extends Thread {
    private final Socket socket;

    public HandlingRequest(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            FileUtils fileUtils = new FileUtils();
            while (true) {
                Data dataFromClient = (Data) ois.readObject();
                Properties type = dataFromClient.getType();
                Object data = dataFromClient.getData();
                if (type.equals(Properties.HEARTBEAT)) {
                    /*
                        nếu Object có type là heartbeat thì phản hồi lại client
                     */
                    System.out.println(data.toString());
                    oos.writeObject(new Data(Properties.HEARTBEAT_RESPONSE, "Server is Alive"));
                } else if (type.equals(Properties.MESSAGE)) {
                    /*
                       nếu Object có type là message thì in ra màn hình server
                      */
                    System.out.println(data);
                } else if (type.equals(Properties.FILE)) {
                    /*
                      nếu Object có type là FILE thì thực hiện hàm nhận file
                     */
                    fileUtils.setByteArray((ByteArray) data);
                    fileUtils.receiveFile();
                } else if (type.equals(Properties.FILE_NAME)) {
                    /*
                        nếu Object có type là fileName
                     */
                    /*
                        Đặt tên file cho biến fileUtils
                     */
                    fileUtils.setFileName((String) data);
                    /*
                     Thực hiện kiểm tra
                     */
                    boolean isExists = fileUtils.checkFileExists();
                    if (isExists) {
                        /*
                         nếu file đã tồn tại bên Server thì trả về Response cho Client
                         */
                        oos.writeObject(new Data(Properties.FILE_EXISTS, "ĐÃ TỒN TẠI FILE"));
                    } else {
                        oos.writeObject(new Data(Properties.FILE_NOT_EXISTS, "SẴN SÀNG"));
                    }
                } else if (type.equals(Properties.FILE_SIZE)) {
                    /*
                     Nếu Object có type là fileSize thì lưu trữ vào fileSize trong fileUtils
                     */
                    fileUtils.setFileSize((Integer) data);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            try {
                System.out.println("Ngắt kết nối với " + socket.getPort());
                socket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}

