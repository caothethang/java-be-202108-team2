package com.team2.Server;

import com.team2.Client.ByteArray;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class xử lý file bên server
 */
public class FileUtils {
    
    private ByteArray byteArray;
    private String fileName;
    private Integer fileSize;
    private Integer fileSizeTemp;
    private OutputStream output;
    
    
    public FileUtils() {
    }
    
    /**
     * Hàm tạo và nhập nơi lưu trữ file
     */
    public void receiveFile() {
        try {
            int bytesRead;
            
            // Tạo nơi lưu trữ file trên server
            String newFileName = "E:\\Server\\" + fileName;
            output = new FileOutputStream(newFileName, true);
            
            /*
              Nhận các mảng byte từ client gửi lên và ghi vào file
             */
            bytesRead = byteArray.getBytes().length;
            output.write(byteArray.getBytes());
            fileSize -= bytesRead;
            
            String strSize;
            double sizeMb;
    
            /*
              Định dạng lại file size
             */
            if (fileSizeTemp > Math.pow(2, 20)) {
                sizeMb = fileSizeTemp / Math.pow(2, 20);
                strSize = sizeMb + " Mb";
            } else if (fileSizeTemp > Math.pow(2, 10)) {
                sizeMb = fileSizeTemp / Math.pow(2, 10);
                strSize = sizeMb + " Kb";
            } else {
                strSize = fileSizeTemp + " Bytes";
            }
            
            /*
             In ra thông báo khi gửi file thành công
             */
            if (fileSize <= 0) {
                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String strDate = formatter.format(date);
                
                System.out.println("File " + fileName + " - " + strSize + " received from client at " + strDate);
            }
            
        } catch (IOException ex) {
            System.err.println("Error." + ex);
            ex.printStackTrace();
        }finally {
            try {
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
    }
    
    /**
     * Kiểm tra file có tồn tại ở server chưa
     */
    public boolean checkFileExists() {
        File file = new File("E:\\Server\\" + fileName);
        return file.exists();
    }
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
        this.fileSizeTemp = fileSize;
    }
    
    public void setByteArray(ByteArray byteArray) {
        this.byteArray = byteArray;
    }
}
