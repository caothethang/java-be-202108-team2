package com.team2.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try {
            /*
              Khởi tạo ServerSocket
             */
            ServerSocket server = new ServerSocket(5000);
            while (true) {
                Socket socket = server.accept();
                System.out.println("Connect with" + socket);

                /*
                  Khởi tạo và chạy luồng HandlingRequest
                 */
                HandlingRequest handlingRequest = new HandlingRequest(socket);
                handlingRequest.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
