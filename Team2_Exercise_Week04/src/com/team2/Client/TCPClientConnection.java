package com.team2.Client;

import com.team2.Client.State.DisconnectedState;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Class thể hiện đối tượng client kết nối đến server
 */
public class TCPClientConnection {
    private int reconnectRetry;
    private TCPClientConnectionState state;
    private Socket socket;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;

    public TCPClientConnection() {
        this.state = new DisconnectedState(this);
        this.reconnectRetry = 1;        // Init Constructor
    }

    public void changeState(TCPClientConnectionState state) {
        this.state = state;
        this.reconnectRetry = 1;
    }

    public TCPClientConnectionState checkConnection() {
        return state.checkConnection();
    }

    public int connect(String serverAddress) {
        return state.connect(serverAddress);
    }

    public int disconnect() {
        return state.disconnect();
    }

    public int send(String sms) {
        return state.send(sms);
    }

    public int getReconnectRetry() {
        return reconnectRetry;
    }

    public void setReconnectRetry(int reconnectRetry) {
        this.reconnectRetry = reconnectRetry;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    public ObjectOutputStream getOos() {
        return oos;
    }

    public void setOos(ObjectOutputStream oos) {
        this.oos = oos;
    }

    public ObjectInputStream getOis() {
        return ois;
    }

    public void setOis(ObjectInputStream ois) {
        this.ois = ois;
    }
}


