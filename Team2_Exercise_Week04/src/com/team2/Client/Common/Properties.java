package com.team2.Client.Common;

public enum Properties {
    // Request type
    HEARTBEAT,MESSAGE,FILE,
    // File details
    FILE_NAME,FILE_SIZE,
    // File status
    FILE_EXISTS,FILE_NOT_EXISTS,
    // Response type
    HEARTBEAT_RESPONSE,
}
