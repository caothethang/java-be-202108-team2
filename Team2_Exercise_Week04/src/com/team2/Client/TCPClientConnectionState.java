package com.team2.Client;

public interface TCPClientConnectionState {

    TCPClientConnectionState checkConnection();

    int connect(String port);

    int disconnect();

    int send (String sms);
}

