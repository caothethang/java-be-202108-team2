package com.team2.Client.State;

import com.team2.Client.TCPClientConnection;
import com.team2.Client.TCPClientConnectionState;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Class thể hiện trạng thái connecting
 */
public class ConnectingState implements TCPClientConnectionState {
    private final TCPClientConnection tcpClientConnection;

    public ConnectingState(TCPClientConnection tcpClientConnection) {
        this.tcpClientConnection = tcpClientConnection;
    }

    /**
     * Check trạng thái connecting
     * @return trả về trạng thái connecting
     */
    @Override
    public TCPClientConnectionState checkConnection() {
        /*
          Lượt đếm hiện tại
          Nếu thử kết nối 5 lần không thành công, chuyển trạng thái sang disconnected
         */
        int count = tcpClientConnection.getReconnectRetry();
        if(count>5){
            tcpClientConnection.changeState(new DisconnectedState(tcpClientConnection));
            return this;
        }
        count++;
        tcpClientConnection.setReconnectRetry(count);
        /*
          Gọi kết nối tới server
          Nếu nhận được số cổng thì xác định đã connect thành công và chuyển sang connected
          Nếu không thì trạng thái vẫn là connecting
         */
        int isConnectSuccess = tcpClientConnection.connect("localhost");

        if (isConnectSuccess != 0) {
            tcpClientConnection.changeState(new ConnectedState(tcpClientConnection));
            return this;
        }
        System.out.println("Connecting state");
        return this;
    }

    /**
     * Check trạng thái kết nối client với server
     * @param serverAddress địa chỉ kết nối
     * @return trả về cổng kết nối
     */
    @Override
    public int connect(String serverAddress) {
        try {
            /*
              Mở kết nối tới server
              Tạo các luồng để gửi dữ liệu và đặt trạng thái là connected
              @return
             */
            Socket socket = new Socket(serverAddress, 5000);
            tcpClientConnection.setSocket(socket);
            tcpClientConnection.setOos(new ObjectOutputStream(socket.getOutputStream()));
            tcpClientConnection.setOis(new ObjectInputStream(socket.getInputStream()));
            tcpClientConnection.changeState(new ConnectedState(tcpClientConnection));
            return socket.getPort();
        } catch (IOException e) {
            return 0;
        }
    }

    @Override
    public int disconnect() {
        try {
            tcpClientConnection.getSocket().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    protected void finalize() {
    }

    @Override
    public int send(String sms) {
        return - 1;
    }

    @Override
    public String toString() {
        return "ConnectingState{}";
    }
}
