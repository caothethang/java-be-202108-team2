package com.team2.Client.State;

import com.team2.Client.TCPClientConnection;
import com.team2.Client.TCPClientConnectionState;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Class thể hiện trạng thái disconnect
 */
public class DisconnectedState implements TCPClientConnectionState {
    private final TCPClientConnection tcpClientConnection;

    public DisconnectedState(TCPClientConnection tcpClientConnection) {
        this.tcpClientConnection = tcpClientConnection;
    }

    @Override
    public TCPClientConnectionState checkConnection() {
        System.out.println("disconnected state");
        int connectStatus = tcpClientConnection.connect("localhost");

        /*
         * Nếu kết nối thành công chuyển trạng thái sang connected
         */
        if (connectStatus != 0) {
            tcpClientConnection.changeState(new ConnectedState(tcpClientConnection));
            return this;
        }

        /*
         Chuyển sang trạng thái connecting và cố gắng kết nối tới server
         */
        tcpClientConnection.changeState(new ConnectingState(tcpClientConnection));
        return this;
    }

    /**
     * Check trạng thái kết nối client với server
     * @param serverAddress địa chỉ kết nối
     * @return trả về cổng kết nối nếu kết nối thành công
     */
    @Override
    public int connect(String serverAddress) {
        try {
            /*
             *  Mở kết nối tới server cổng 5000
             *  Mở các luồng kết nối ObjectOutputStream tới server
             */
            Socket socket = new Socket(serverAddress, 5000);
            tcpClientConnection.changeState(new ConnectedState(tcpClientConnection));
            tcpClientConnection.setSocket(socket);
            tcpClientConnection.setOos(new ObjectOutputStream(socket.getOutputStream()));
            tcpClientConnection.setOis(new ObjectInputStream(socket.getInputStream()));
            return socket.getPort();
        } catch (IOException e) {
            return 0;
        }
    }

    @Override
    public int disconnect() {
        return 0;
    }

    @Override
    protected void finalize() {
    }

    @Override
    public int send(String sms) {
        return -1;
    }

    @Override
    public String toString() {
        return "DisconnectedState{}";
    }
}


