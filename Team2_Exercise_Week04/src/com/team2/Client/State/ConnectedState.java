package com.team2.Client.State;

import com.team2.Client.Common.Properties;
import com.team2.Client.File.PushFile;
import com.team2.Client.File.ReadFile;
import com.team2.Client.TCPClientConnection;
import com.team2.Client.TCPClientConnectionState;
import com.team2.DataTransfer.Data;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class thể hiện trạng thái connected
 */
public class ConnectedState implements TCPClientConnectionState {
    
    // Biến lưu trữ size của queue
    public static final int QUEUE_SIZE = 1000;
    // Queue chứa các object gửi từ client lên server
    private final ArrayBlockingQueue<Data> queue = new ArrayBlockingQueue<>(QUEUE_SIZE);
    
    private ObjectOutputStream oos;
    private ObjectInputStream ois;
    
    private final Scanner scanner;
    private final TCPClientConnection tcpClientConnection;
    
    public ConnectedState(TCPClientConnection tcpClientConnection) {
        this.tcpClientConnection = tcpClientConnection;
        scanner = new Scanner(System.in);
    }
    
    /**
     * Hàm check trạng thái kết nối hiện tại
     *
     * @return TCPClientConnectionState
     */
    @Override
    public TCPClientConnectionState checkConnection() {
        System.out.println("Connected state");
        try {
            oos = tcpClientConnection.getOos();
            ois = tcpClientConnection.getOis();
            
            /*
              Gửi heartbeat để kiểm tra xem client còn kết nối với server không
             */
            oos.writeObject(new Data(Properties.HEARTBEAT, "Hello is alive ?"));
            
            /*
              Server tiếp nhận dữ liệu từ luồng ObjectInputStream
              Server gửi lại phản hồi và kiểm tra
             */
            Data data = (Data) ois.readObject();
            Properties type = data.getType();
            Object dataFromServer = data.getData();
            
            /*
              Nếu phản hồi còn kết nối thì in ra bảng lựa chọn
             */
            if (type.equals(Properties.HEARTBEAT_RESPONSE)) {
                System.out.println(dataFromServer.toString());
                actionOption();
            }
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Disconnect from server");
        }
        return this;
    }
    
    @Override
    public int connect(String serverAddress) {
        return - 1;
    }
    
    /**
     * Hàm dừng kết nối
     *
     * @return int
     */
    @Override
    public int disconnect() {
        try {
            oos.close();
            ois.close();
            tcpClientConnection.getSocket().close();
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    @Override
    protected void finalize() {
    
    }
    
    /**
     * Hàm gửi message lên server
     *
     * @param sms nội dung tin nhắn
     * @return 1 nếu gửi thành công
     */
    @Override
    public int send(String sms) {
        try {
            // Gửi tin nhắn lên server
            oos.writeObject(new Data(Properties.MESSAGE, sms));
            return 1;
        } catch (IOException e) {
            // Nếu lỗi thì chuyển sang trạng thái disconnected
            tcpClientConnection.changeState(new DisconnectedState(tcpClientConnection));
        }
        return - 1;
    }
    
    /**
     * Lựa chọn cách gửi từ client đến server
     */
    public void actionOption() {
        System.out.println("Client đã được kết nối tới server");
        System.out.println("Nhập lựa chọn của bạn: ");
        System.out.println("1.Gửi tin nhắn ");
        System.out.println("2.Gửi file ");
        System.out.println("3.Thoát ");
        int choose = scanner.nextInt();
        scanner.nextLine();
        switch (choose) {
            case 1:
                System.out.println("Nhập tin nhắn: ");
                String sms = scanner.nextLine();
                tcpClientConnection.send(sms);
                break;
            case 2:
                System.out.println("Nhập tên file : ");
                String fileName = scanner.nextLine();
                // Kiểm tra xem file có tồn tại bên client không
                if (checkFileFromClient(fileName)) {
                    // Nếu file tồn tại bên client thì kiểm tra xem file có tồn tại bên server không
                    if (checkFileFromServer(fileName)) {
                        // Nếu file chưa tồn tại bên server thì thực hiện gửi file
                        sendFile(fileName);
                    }
                }
                break;
            case 3:
                tcpClientConnection.disconnect();
                break;
        }
    }
    
    /**
     * check phản hồi từ server
     *
     * @param fileName tên file
     * @return trả về false nếu file đã tồn tại và ngược lại
     */
    public boolean checkFileFromServer(String fileName) {
        try {
            // Gửi tên file lên cho server
            oos.writeObject(new Data(Properties.FILE_NAME, fileName));
            // Server trả về phản hồi
            Data response = (Data) ois.readObject();
            if (response.getType().equals(Properties.FILE_EXISTS)) {
                System.out.println(response.getData().toString());
                return false;
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }
    
    /**
     * Kiểm tra file đã tồn tại bên client
     */
    public boolean checkFileFromClient(String fileName) {
        try {
            String newFileName = "D:\\Client\\" + fileName;
            File myFile = new File(newFileName);
            if (myFile.exists()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("File không tồn tại bên client");
            return false;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return "ConnectedState{}";
    }
    
    
    /**
     * Hàm gửi file từ client đến server
     *
     * @param fileName tên file
     */
    public void sendFile(String fileName) {
        try {
            String newFileName = "D:\\Client\\" + fileName;
            ObjectOutputStream oos = tcpClientConnection.getOos();
            File myFile = new File(newFileName);
            
            long fileLength = myFile.length();
            
            FileInputStream fis = new FileInputStream(myFile);
            BufferedInputStream bis = new BufferedInputStream(fis);
            DataInputStream dis = new DataInputStream(bis);
            
            // Gửi lên server size của file
            oos.writeObject(new Data(Properties.FILE_SIZE, ((int) fileLength)));
            
            // Tạo Thread pool chứa Thread đọc file và gửi file lên server
            ExecutorService threadPool = Executors.newFixedThreadPool(2);
            
            // Khởi tạo và chạy thread ReadFile
            ReadFile readFile = new ReadFile(queue, dis, fileLength);
            threadPool.execute(readFile);
            
            // Khởi tạo và chạy thread PushFile
            PushFile pushFile = new PushFile(queue, oos, fileLength);
            threadPool.execute(pushFile);
            
            threadPool.shutdown();
            
            // Chờ Thread pool chạy xong
            while (! threadPool.isTerminated()) {
//                    System.out.println(".");
            }
            String strSize;
            double sizeMb;
            
            // Format file size
            if (fileLength > Math.pow(2, 20)) {
                sizeMb = fileLength / Math.pow(2, 20);
                strSize = sizeMb + " Mb";
            } else if (fileLength > Math.pow(2, 10)) {
                sizeMb = fileLength / Math.pow(2, 10);
                strSize = sizeMb + " Kb";
            } else {
                strSize = fileLength + " Bytes";
            }
            
            //Lấy thời gian hiện tại
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String strDate = formatter.format(date);
            
            // In ra thời điểm gửi file
            System.out.println("File " + fileName + " - " + strSize + " send to server at " + strDate);
        } catch (Exception e) {
            System.err.println("ERROR! " + e);
            e.printStackTrace();
        }
    }
}

