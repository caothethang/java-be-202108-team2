package com.team2.Client;

import java.util.TimerTask;

/**
 * class thực thi check connection
 */
public class CheckConnectionTask extends TimerTask {

    private final TCPClientConnection tcpClientConnection;
    private TCPClientConnectionState tcpClientConnectionState;

    public CheckConnectionTask(TCPClientConnection tcpClientConnection) {
        this.tcpClientConnection = tcpClientConnection;
    }

    @Override
    public void run() {
        tcpClientConnectionState = tcpClientConnection.checkConnection();
    }
}

