package com.team2.Client;

import java.util.Timer;

/**
 * Clas client
 */
public class Client {
    public static void main(String[] args) {
        /*
          Khởi tạo tcpClientConnection và checkConnectionTask
         */
        TCPClientConnection tcpClientConnection = new TCPClientConnection();
        CheckConnectionTask checkConnectionTask = new CheckConnectionTask(tcpClientConnection);

        /*
          Chạy check connection mỗi 2s
         */
        Timer timer = new Timer();
        timer.schedule(checkConnectionTask, 0, 2000);
    }
}
