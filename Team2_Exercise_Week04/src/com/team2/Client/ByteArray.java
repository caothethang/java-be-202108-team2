package com.team2.Client;

import java.io.Serializable;

/**
 * Class lưu trữ byte từ việc đọc file và gửi lên server
 */
public class ByteArray implements Serializable {

    private final byte[] bytes;

    public ByteArray(byte[] bytes) {
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }
}
