package com.team2.Client.File;

import com.team2.Client.ByteArray;
import com.team2.Client.Common.Properties;
import com.team2.Client.State.ConnectedState;
import com.team2.DataTransfer.Data;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;

public class ReadFile extends Thread {
    private ArrayBlockingQueue<Data> queue;
    private DataInputStream dis;
    private long size;

    public ReadFile(ArrayBlockingQueue<Data> queue, DataInputStream dis, long size) {
        this.queue = queue;
        this.dis = dis;
        this.size = size;
    }

    @Override
    public void run() {
        byte[] buffer = new byte[10240];
        int bytesRead;
        try {
            while (size > 0) {
                // Kiểm tra queue đầy
                if (queue.size() >= ConnectedState.QUEUE_SIZE) {
                    System.out.println("Queue is full. Wait ...");
                    continue;
                } else {
                    // Đọc vào mảng buffer sô lượng byte bằng số bé nhất giữa buffer length và size hiện tại của file
                    bytesRead = dis.read(buffer, 0, (int) Math.min(buffer.length, size));
                    if ((bytesRead) != -1) {
                        // Khởi tạo byteArray thông qua buffer và thêm vào queue
                        ByteArray byteArray = new ByteArray(buffer);
                        queue.add(new Data(Properties.FILE, byteArray));

                        // Giảm số size của file băng số bytes đã thêm vào queue và khởi tạo lại buffer
                        size -= bytesRead;
                        buffer = new byte[(int) Math.min(buffer.length, size)];
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                dis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
