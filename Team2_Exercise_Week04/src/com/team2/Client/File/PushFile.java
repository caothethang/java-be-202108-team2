package com.team2.Client.File;

import com.team2.Client.ByteArray;
import com.team2.DataTransfer.Data;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Class thực thi luồng gửi file lên server
 */
public class PushFile extends Thread {
    private final ArrayBlockingQueue<Data> queue;
    private final ObjectOutputStream oos;
    private long size;

    public PushFile(ArrayBlockingQueue<Data> queue, ObjectOutputStream oos, long size) {
        this.queue = queue;
        this.oos = oos;
        this.size = size;
    }

    @Override
    public void run() {
        while (size > 0) {
            try {
                /*
                 Kiểm tra queue rỗng
                 */
                if (queue.isEmpty()) {
                    System.out.println("Queue is empty. Wait ...");
                } else {
                    /*
                     *   Lấy data từ queue và ghi vào ObjectOutputStream
                     */
                    Data fileByte = queue.poll();
                    oos.writeObject(fileByte);

                    /*
                     * Giảm số size bằng đúng sô byte đã ghi
                     */
                    size -= ((ByteArray) (fileByte.getData())).getBytes().length;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
