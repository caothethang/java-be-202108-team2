package com.team2.DataTransfer;

import com.team2.Client.Common.Properties;

import java.io.Serializable;

/**
 *  Object dùng trao đổi giữa Client và Server
 */
public class Data implements Serializable {
    private Properties type;
    private Object data;
    
    public Data(Properties type, Object data) {
        this.type = type;
        this.data = data;
    }
    
    public Properties getType() {
        return type;
    }
    
    public Object getData() {
        return data;
    }
    
}
